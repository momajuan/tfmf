#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 20:11:30 2021

@author: jj
"""

file_name="/home/jj/Documents/tfm/mapasyrutas/valencia.add.valenciaATA.xml"
dom=ET.parse(file_name)
root = dom.getroot()
v=root.findall('./vehicle')
# %%
for i in v:
    if i.attrib["route"] in dicc_compar:
        # pass
        # break
        i.set("depart",str(float(i.attrib["depart"])+20* float(dicc_compar[i.attrib["route"]]["n_aris_antes"])))
        i.set("route", dicc_compar[i.attrib["route"]]["id"])
    else:
        root.remove(i)
        

# %%
v=root.findall('./vehicle')

b=root.findall("./routeDistribution")

for elm_routdist in b:
    k=elm_routdist.findall("./route")
    for l in k:
        if l.attrib["refId"] in dicc_compar:
            l.set("refId", dicc_compar[l.attrib["refId"]]["id"])
        else:
            elm_routdist.remove(l)
    
  
v=root.findall('./vehicle')

b=root.findall("./routeDistribution")

for elm_routdist in b:
    k=elm_routdist.findall("./route")
    if len(k)==0:
        root.remove(elm_routdist)
# %%


dom.write("/home/jj/Documents/tfm/mapasyrutas/rutas_nuevas.Add.xml") 
