#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 13:46:49 2021

@author: jj
"""

# %%
import os
import pandas as pd
import numpy as np
from xml.etree import ElementTree as ET
# Import required package
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
import networkx as nx
from networkx.algorithms.community import greedy_modularity_communities
import matplotlib.pyplot as plt 
from collections import Counter
file_name='/home/jj/Documents/tfm/mapasyrutas/valencia1.net.xml'
dom=ET.parse(file_name)
root = dom.getroot()
edgesmap=[]
v=root.findall('./edge')
for elm in v:
    edgesmap.append(str(elm.attrib["id"]))

file_name_rutas='/home/jj/Documents/tfm/mapasyrutas/valencia.rou.valenciaATA.xml'
dom_rutas=ET.parse(file_name_rutas)
root_rutas = dom_rutas.getroot()
v=root_rutas.findall('./route')
rutas=[]
j=0
v=root_rutas.findall('./route')
for elm in v:
    j+=1
    ruta=elm.attrib["edges"]
    ruta=ruta.split()
    ruta_nueva=[]
    for i in range(len(ruta)):
        if ruta[i] in edgesmap:
            ruta_nueva.append(ruta[i])
        elif len(ruta_nueva)!=0:
            i=i-1
            break
    if len(ruta_nueva)==0 or len(ruta_nueva)==1:
        root_rutas.remove(elm)
    else:
        elm.set("edges", " ".join(ruta_nueva))
        elm.set("n_aris_ant", str(i- len(ruta_nueva)+1))
        elm.set("id_viejo", elm.attrib["id"])
        elm.set("id", "_to_".join([ruta_nueva[0], ruta_nueva[-1]]))
        
            

# %%
rutas_nuevas=[]
for i in dom_rutas.findall('./route'):
    rutas_nuevas.append(i.attrib["id"])
    
num_repetidas=Counter(rutas_nuevas)
v=dom_rutas.findall('./route')
         
dicc_compar={}
   
dic_rutas_repetidas={}
for i in v:
    if num_repetidas[i.attrib["id"]]>1:
        if (i.attrib["id"] in dic_rutas_repetidas.keys())== False:
            dic_rutas_repetidas.update({i.attrib["id"]:0})
        else:
            dic_rutas_repetidas[i.attrib["id"]]+=1
        i.set("id", i.attrib["id"]+"#"+str(dic_rutas_repetidas[i.attrib["id"]]))
    dicc_compar.update({i.attrib["id_viejo"]:{"id":i.attrib["id"],"n_aris_antes":i.attrib["n_aris_ant"]}})

dom_rutas.write("/home/jj/Documents/tfm/mapasyrutas/rutas_nuevas.xml") 

