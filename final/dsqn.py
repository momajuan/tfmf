#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  2 18:35:30 2022

@author: jj
"""
from __future__ import absolute_import
from __future__ import print_function
exec(open("/home/jj/Documents/tfm/Extraccion1.py").read());
import os
import pandas as pd
from xml.etree import ElementTree as ET
# Import required package
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
import networkx as nx
from networkx.algorithms.community import greedy_modularity_communities
import matplotlib.pyplot as plt 
from scipy.stats import truncnorm
import os
import sys
import optparse
import random
import time
import concurrent.futures
import copy
import shutil
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable Juan 'SUMO_HOME'")
# %%
from sumolib import checkBinary  # noqa
import traci  # noqa

import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt 
from collections import namedtuple
from itertools import count
from PIL import Image
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
from random import randrange, uniform
import itertools
# we need to import python modules from the $SUMO_HOME/tools directory
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable Juan 'SUMO_HOME'")
lr=0.001
device="cuda"
def run():
    """execute the TraCI control loop"""
    step = 0
    while traci.simulation.getMinExpectedNumber() > 0:
        traci.simulationStep()

        step += 1
    traci.close()
def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


class ReplayMemory():
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.push_count = 0 # How many exps have been added to memo

    def can_provide_sample(self, batch_size):
        return len(self.memory) >= batch_size

    def push(self, experience):
        if len(self.memory) < self.capacity:
            self.memory.append(experience)
        else:
            self.memory[self.push_count % self.capacity] = experience
        self.push_count += 1
    
    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

class Target(nn.Module):
       
   def __init__(self,m,n):
       super().__init__()
       self.device= torch.device("cuda" if torch.cuda.is_available() else "cpu")
       self.memory=ReplayMemory(10000)

       c1=34
       c2=64 
       ## Aproximador
       self.fc1 = nn.Linear(in_features=m, out_features=c1)  
       self.fc2 = nn.Linear(in_features=c1, out_features=c2)
       self.out = nn.Linear(in_features=c2, out_features=n)
       self.optimizer = optim.Adam(params=self.parameters(), lr=lr) 
     #     self.estado.append(selfsita)
       # in_features=


   def forward(self, t):
       t = t.flatten(start_dim=1)
       t = F.relu(self.fc1(t))
       t = F.relu(self.fc2(t))
       t = self.out(t)     
       return t        
# %%   
class Extr(nn.Module):
    
   def __init__(self,sem,jm,df,df_tl,root,dedges):
       super().__init__()
       self.accionelegida=random.choice(range(4))
       self.traficointerseccion=0
       self.sem=sem #Pasamos la interseccion a la clase
       self.nombre=self.sem.attrib["id"]
       self.vinc=[] # Vecinos incidentes
       for i in jm[:,df.loc[df['id']==self.sem.attrib['id']].index[0]].nonzero()[0]:
           if i<df_tl.shape[0]:
               self.vinc.append(df_tl.iloc[i][0])
               for j in jm[:,df.loc[df['id']==df_tl.iloc[i][0]].index[0]].nonzero()[0]:
                   if j<df_tl.shape[0]:
                       self.vinc.append(df_tl.iloc[i][0])
                            
       self.vout=[] # Vecinos incidentes
       self.acciones=[]
       for i in jm[df.loc[df['id']==self.sem.attrib['id']].index[0],:].nonzero()[1]:
           if i<df_tl.shape[0]:
               self.vout.append(df_tl.iloc[i][0])
               for j in jm[:,df.loc[df['id']==df_tl.iloc[i][0]].index[0]].nonzero()[0]:
                   if j<df_tl.shape[0]:
                       self.vout.append(df_tl.iloc[i][0])
       self.interentrada=set(self.vout+self.vinc)
       self.trafico=[0 for i in range(len(self.interentrada)+1)]
           
       ## Aproximador
       self.entradared=2*len(self.interentrada)+2
       c1=34
       c2=64
       self.cout=4 #numero de acciones
       self.fc1 = nn.Linear(in_features=self.entradared, out_features=c1)  
       self.fc2 = nn.Linear(in_features=c1, out_features=c2)
       self.out = nn.Linear(in_features=c2, out_features=self.cout)
       
       self.tiempoespera=0
       self.tiempoesperares=0
       self.tecoloracion={}
       self.Carreteras(dedges)
       self.memory=ReplayMemory(10000)       #     self.estado.append(selfsita)
       # in_features=


   def forward(self, t):
       t = t.flatten(start_dim=1)
       t = F.relu(self.fc1(t))
       t = F.relu(self.fc2(t))
       t = self.out(t)     
       return t        


        


## Extraccion de acciones posibles. Estas vienen dadas en forma de lista, donde cada entrada es un diccionario, el cual contine 
# suma que se le tiene que hacer a cada fase, menos a la ultima.



   def select_action(self,red,rate,Estado,Redes, tru=True,evaluacionred=False,anterior=False):
       if tru==True:
           self.fasesantiguas=self.fases
           self.accionant=self.accionelegida
       self.fases=[self.accionelegida,Estado[self.nombre]['trafico']]
       for i in self.interentrada:
           self.fases+=[Estado[i]['trafico'],Estado[i]['accion']]
       if rate > random.random() and evaluacionred==False and anterior==False: 
           self.accionelegida=random.choice(range(4))
           Estado[self.nombre].update({'accion':self.accionelegida})
           return Estado# explore   
           # time.sleep(2) 
       elif evaluacionred==False and anterior==False:
           with torch.no_grad(): 
               self.accionelegida=int(Redes[red][self.nombre].forward(torch.FloatTensor([self.fases]).to(device)).argmax()) # exploit 
            
           Estado[self.nombre].update({'accion':self.accionelegida})
           return Estado    #self.accionelegida=self.acciored[int(policy_net(state).argmax(dim=1).to(self.device)[0])] # exploit 
       elif evaluacionred==True and anterior==False:
           with torch.no_grad(): 
               self.accionelegida=int((Redes[0][self.nombre].forward(torch.FloatTensor([self.fases]).to(device))+Redes[1][self.nombre].forward(torch.FloatTensor([self.fases]).to(device))).argmax()) # exploit 
            
           Estado[self.nombre].update({'accion':self.accionelegida})
           return Estado
       elif anterior==True:
           return Estado
           
   def inicio(self, strategy, num_actions, device):
       self.current_step = 0
       self.strategy = strategy
       self.num_actions = num_actions
       self.device = device
                 
   def get_options():
      optParser = optparse.OptionParser()
      optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
      options, args = optParser.parse_args()
      return options   
    
       
   def Carreteras(self,dedges):
       self.aristas=[]
       for i in self.vinc:
           self.aristas.append(dedges.at[dedges.loc[(dedges["from"]==i)&(dedges["to"]==self.nombre)].index[0],"id"])
           
   def Tiempo_espera_interseccion(self):
       for i in self.aristas:
           self.tiempoespera+=traci.edge.getWaitingTime(i)
           self.tiempoesperares+=traci.edge.getWaitingTime(i)
           
   def Recompensa(self,Sems):
       self.recom=-self.tiempoespera
       for i in self.vinc:
           self.recom+=-Sems[i].tiempoespera/len(self.vinc)
       for i in self.vout:
           self.recom+=-Sems[i].tiempoespera/len(self.vout)
       return self.recom
# %%   
def Extraccion(nombre_mapa): 
    dom=ET.parse(nombre_mapa)
    root = dom.getroot()
    df=pd.DataFrame(columns=['id','x','y'])
    junction=pd.DataFrame(columns=['id','x','y'])
    ## Junctions
    v=root.findall('./junction[@shape]')
    for elm in v:
        junction=junction.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)
    
    ## Traffic light
    v=root.findall('./junction[@type="traffic_light"]')
    for elm in v:
        df=df.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)
    df_tl=df.copy()
    for elm in root.findall('./edge[@to]'):
        if df_tl.loc[df_tl['id'] == elm.attrib["to"]].empty  == False:
            df=df.append({'id': elm.attrib["from"]}, ignore_index=True)
    df=df.drop_duplicates(subset=['id'], keep='first')
    df=df.reset_index()
    
    
    
    m=df_tl.shape[0]
    n=df.shape[0]
    Matriz_semaforos = csr_matrix((m,m ),dtype = np.int8).toarray()
    Matriz_semaforos=coo_matrix(Matriz_semaforos)
    Matriz_semaforos=Matriz_semaforos.tocsr()
    
    for elm in root.findall('./edge[@to]'):
        if df_tl.loc[df_tl['id'] == elm.attrib["from"]].empty == False and df_tl.loc[df_tl['id'] == elm.attrib["to"]].empty==False:
    
            Matriz_semaforos[ df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0],df_tl.loc[df_tl['id']==elm.attrib["to"]].index[0]]=1
            # js[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
            # jl[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['length'])
            # jv[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['speed'])
            # jc[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
    
    ##Añadir x, y
    for i in list(df.id):
        df.loc[df['id']==i,'x':'y']=junction.loc[junction['id']==i,'x'].iloc[0],junction.loc[junction['id']==i,'y'].iloc[0]
    ## Matriz de Conexion de intersecciones
    JM = csr_matrix((n, n),dtype = np.int8).toarray()
    ## Matriz de conexion con numero de carriles
    JC = csr_matrix((n, n),dtype = np.int8).toarray()
    ## Matriz de posicion de semaforo en la intersecciones
    JS = csr_matrix((n, n),dtype = np.int8).toarray()
    ## Matriz de longitud de segmento
    JL = csr_matrix((n, n),dtype = np.float).toarray()
    ## Matriz de velocidad
    JV = csr_matrix((n, n),dtype = np.float).toarray()
    ## Matriz de velocidad
    JI = csr_matrix((n, n),dtype = np.float).toarray()
    JM=coo_matrix(JM)
    JC=coo_matrix(JC)
    JS=coo_matrix(JS)
    JL=coo_matrix(JL)
    JV=coo_matrix(JV)
    JI=coo_matrix(JI)
    jm=JM.tocsr()
    js=JS.tocsr()
    jl=JL.tocsr()
    jv=JV.tocsr()
    ji=JI.tocsr()
    jc=JC.tocsr()
    dedges=pd.DataFrame(columns=['id','from','to','index'])
    for elm in root.findall('./edge[@from]'):
        dedges=dedges.append({'id': elm.attrib["id"],'from':elm.attrib["from"],'to':elm.attrib["to"],'index':[]}, ignore_index=True)
    for elm in root.findall('./edge[@to]'):
        if df.loc[df['id'] == elm.attrib["from"]].empty == False and df.loc[df['id'] == elm.attrib["to"]].empty==False:
    
            jm[ df.loc[df['id']==elm.attrib["from"]].index[0],df.loc[df['id']==elm.attrib["to"]].index[0]]=1
            # js[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
            # jl[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['length'])
            # jv[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['speed'])
            # jc[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
    
    for elm in root.findall('./connection[@tl]'):
        dedges.iloc[dedges.iloc[(dedges['id'] == elm.attrib["from"]).values,[3]].index[0]][3].append(elm.attrib["linkIndex"])

    dedges.iloc[1][3].append(1)
    def inverse_mapping(f):
        return f.__class__(map(reversed, f.items()))
    
    G=nx.from_scipy_sparse_matrix(Matriz_semaforos, parallel_edges=False, create_using=nx.DiGraph)
    listcoloring=nx.coloring.greedy_color(G)
    b=nx.coloring.greedy_color(G)
    modul1=greedy_modularity_communities(G.to_undirected())
    modul2=[]
    for i in modul1:
        modul2.append(list(i))
    for k in range(len(modul2)):
        for i in range(len(modul2[k])):
            modul2[k][i]=df_tl.iloc[i][0]
            
    for k in b:
        listcoloring[df_tl.iloc[k][0]]=listcoloring.pop(k)
    
    coloringlist = {}
    for k, v in listcoloring.items():
        coloringlist[v] = coloringlist.get(v, []) + [k]
    return jm,df,df_tl,root,dedges,coloringlist,modul2

class EpsilonGreedyStrategy():
    def __init__(self, start, end, decay):
        self.start = start
        self.end = end
        self.decay = decay
        
    def get_exploration_rate(self, current_step):
        return self.end + (self.start - self.end) * \
        math.exp(-1. * current_step * self.decay)
Experience = namedtuple(     'Experience',     ('state', 'action', 'next_state', 'reward') ) 
def extract_tensors(experiences): 
    # Convert batch of Experiences to Experience of batches 
    batch = Experience(*zip(*experiences)) 
 
    t1 = torch.cat(batch.state) 
    t2 = torch.cat(batch.action) 
    t3 = torch.cat(batch.reward) 
    t4 = torch.cat(batch.next_state) 
 
    return (t1,t2,t3,t4) 
class QValues(): 
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu") 
    @staticmethod 
    def get_current(policy_net, states, actions): 
        return policy_net(states).gather(dim=1, index=actions.unsqueeze(-1)) 
    @staticmethod         
    def get_next(target_net, next_states):                 
        final_state_locations = False 
        non_final_state_locations = (final_state_locations == False) 
        non_final_states = next_states[non_final_state_locations] 
        batch_size = next_states.shape[0] 
        values = torch.zeros(batch_size).to("cuda") 
        values[non_final_state_locations] = target_net(non_final_states).max(dim=1)[0].detach() 
        return values     
# %%
def sumo(conection,df_tl, Sems,coloringlist,file_name,Estado,Redes,Targets,r1,r2):
    Experience = namedtuple(     'Experience',     ('state', 'action', 'next_state', 'reward') ) 
    # print("Hello")
    # time.sleep(1)
    batch_size = 256 
    gamma = 0.95 
    eps_start = 1 
    eps_end = 0.04 
    eps_decay = 0.003
    target_update = 20 
    memory_size = 100000 
    lr = 0.001 
    num_episodes = 10000 
     
    getrate=EpsilonGreedyStrategy(eps_start,eps_end,eps_decay) 
    
    a={}
    traci.start(["sumo", "-c", file_name,'--no-warnings',   "--no-step-log",
                                  "--tripinfo-output", "tripinfo.xml"],label=conection)
    con=traci.getConnection(conection);
    for j in Sems:
        con.trafficlight.setProgram(j,'sec'+str(Sems[j].accionelegida))
    step = 0
    a={}
    contadorciclos=0
    while 300> step:
        rate=getrate.get_exploration_rate(step+r1*4*(15000/90)+r2*15000/90)


        if step%90==0 and step!=0:
            red=random.choice([0,1])
            for j in Sems:
                Estado[j].update({'trafico':Sems[j].tiempoespera})
            for j in coloringlist[contadorciclos%len(coloringlist)]:
                Estado=Sems[j].select_action(red,rate,Estado,Redes)
                con.trafficlight.setProgram(j,'sec'+str(Sems[j].accionelegida))
            
            for color in coloringlist:
                if color!=contadorciclos%len(coloringlist):
                    for j in coloringlist[color]:
                        Estado=Sems[j].select_action(red,0,Estado,Redes,anterior=True)
                        con.trafficlight.setProgram(j,'sec'+str(Sems[j].accionelegida))
            for j in Sems:
                Redes[red][j].memory.push(Experience(torch.FloatTensor([Sems[j].fasesantiguas]).to("cuda"),torch.FloatTensor( [Sems[j].accionant]).type(torch.int64).to("cuda"), torch.FloatTensor([Sems[j].fases]).to("cuda"),torch.FloatTensor( [Sems[j].Recompensa(Sems)]).to("cuda")))         
                if Redes[red][j].memory.can_provide_sample(batch_size): 
                    experiences = Redes[red][j].memory.sample(batch_size) 
                    states, actions, rewards, next_states = extract_tensors(experiences) 
                    current_q_values = QValues.get_current(Redes[red][j], states, actions) 
                    next_q_values = QValues.get_next(Targets[(red+1)%2][j], next_states) 
                    target_q_values = (next_q_values * gamma) + rewards 
                    Redes[red][j].loss = F.mse_loss(current_q_values, target_q_values.unsqueeze(1)) 
                    Redes[red][j].optimizer.zero_grad() 
                    Redes[red][j].loss.backward() 
                    Redes[red][j].optimizer.step() 
                if (contadorciclos+1) % target_update == 0: 
                    Targets[red][j].load_state_dict(Redes[red][j].state_dict())         
            contadorciclos+=1  
            for i in Sems:
                Sems[i].tiempoespera=0
        con.simulationStep();
        step += 1
        for i in df_tl["id"]:
            Sems[i].Tiempo_espera_interseccion();


    traci.switch(conection)
    traci.close()
    b=0
    for i in Sems:
        b+=Sems[i].tiempoesperares
        Sems[i].tiempoesperares=0
        Sems[i].tiempoespera=0
    return Redes,Targets,Sems,b;
def helper(n):
    return sumo(n[0], n[1],n[2],n[3],n[4],n[5],n[6],n[7],n[8],);


 
def argmax(lst):
  return lst.index(max(lst))


# %% 
def main():
    nombre_mapa='/home/jj/Documents/tfm/mapasyrutas/valencia1.net.xml'
    jm,df,df_tl,root,dedges,coloringlist,modul2=Extraccion(nombre_mapa)
    Sems={} 
    Tar1={} 
    Tar2={} 
    Red1={} 
    Red2={} 
    Redes={} 
    Targets={}
    for i in df_tl["id"]: 
        a='./tlLogic[@id="'+i+'"]' 
        Sems.update({i:Extr(root.findall(a)[0],jm,df,df_tl,root,dedges)})
        Tar1.update({i:Target(Sems[i].entradared,Sems[i].cout).to(device)}) 
        Tar2.update({i:Target(Sems[i].entradared,Sems[i].cout).to(device)})  
        Red1.update({i:Target(Sems[i].entradared,Sems[i].cout).to(device)}) 
        Red2.update({i:Target(Sems[i].entradared,Sems[i].cout).to(device)}) 

    Redes.update({0:Red1}) 
    Redes.update({1:Red2}) 
    Targets.update({0:Tar1}) 
    Targets.update({1:Tar2})
    if os.path.isfile('/home/jj/Documents/tfm/final/Secuenciaciones/dqn'+str(Redes[0][list(Redes[0])[0]])+'.sumocfg')==True:
        for i in Redes: 
            for j in Redes[i]: 
                Path="/home/jj/Documents/tfm/final/Redes/red"+str(i)+"param"+j+".pth" 
                torch.load_state_dict(torch.load(Path, map_location="cuda:0")) 
    Estado={}
    for i in Sems:
        Estado.update({i:{'trafico':Sems[i].traficointerseccion,'accion':Sems[i].accionelegida}})
    for i in Sems:
        Estado=Sems[i].select_action(1,0,Estado,Redes,'h')
    data=[]
    for k in range(15):
        for i in range(4):
            file_name='/home/jj/Documents/tfm/final/Secuenciaciones/dqn'+str(i)+'.sumocfg'
            Redes,Targets,Sems,res=sumo('conection',df_tl, Sems,coloringlist,file_name,Estado,Redes,Targets,i,k)
            data.append([k,i,res])
            print(data)
    for i in Redes: 
        for j in Redes[i]: 
            Path="/home/jj/Documents/tfm/final/Redes/red"+str(i)+"param"+j+".pth" 
            torch.save(Redes[i][j].state_dict(), Path) 
main()
    