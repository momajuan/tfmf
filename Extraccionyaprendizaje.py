#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 10:38:07 2021

@author: jj
"""
from __future__ import absolute_import
from __future__ import print_function

import os
import pandas as pd
from xml.etree import ElementTree as ET
# Import required package
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
import networkx as nx
from networkx.algorithms.community import greedy_modularity_communities
import matplotlib.pyplot as plt 

import os
import sys
import optparse
import random
import time
import concurrent.futures
import copy
import shutil
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable Juan 'SUMO_HOME'")
# %%
from sumolib import checkBinary  # noqa
import traci  # noqa

import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt 
from collections import namedtuple
from itertools import count
from PIL import Image
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
from random import randrange, uniform
import itertools
# we need to import python modules from the $SUMO_HOME/tools directory
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable Juan 'SUMO_HOME'")
lr=0.001
device="cuda"
def run():
    """execute the TraCI control loop"""
    step = 0
    print("Hola")
    while traci.simulation.getMinExpectedNumber() > 0:
        print("Hola1")
        traci.simulationStep()

        step += 1
    traci.close()
def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


class ReplayMemory():
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.push_count = 0 # How many exps have been added to memo

    def can_provide_sample(self, batch_size):
        return len(self.memory) >= batch_size

    def push(self, experience):
        if len(self.memory) < self.capacity:
            self.memory.append(experience)
        else:
            self.memory[self.push_count % self.capacity] = experience
        self.push_count += 1
    
    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)
class Target(nn.Module):
       
   def __init__(self,m,n):
       super().__init__()
       self.device= torch.device("cuda" if torch.cuda.is_available() else "cpu")
       self.memory=ReplayMemory(10000)


       ## Aproximador
       self.fc1 = nn.Linear(in_features=m, out_features=2*m)  
       self.fc2 = nn.Linear(in_features=2*m, out_features=2*n)
       self.fc3 = nn.Linear(in_features=2*n, out_features=2*n)
       self.out = nn.Linear(in_features=2*n, out_features=n)
       self.optimizer = optim.Adam(params=self.parameters(), lr=lr)

   def forward(self, t):
       t = t.flatten(start_dim=1)
       t = F.relu(self.fc1(t))
       t = F.relu(self.fc2(t))
       t = F.relu(self.fc3(t))
       t = self.out(t)     
       return t    
## Extracciones de 
           
# %%   
class Extr(nn.Module):
    
   def __init__(self,sem,desfa,jm,df,df_tl,root,dedges):
       super().__init__()
       self.device= torch.device("cuda" if torch.cuda.is_available() else "cpu")
       self.sem=sem #Pasamos la interseccion a la clase
       self.nombre=self.sem.attrib["id"]
       self.vinc=[] # Vecinos incidentes
       for i in jm[:,df.loc[df['id']==self.sem.attrib['id']].index[0]].nonzero()[0]:
           if i<df_tl.shape[0]:
               self.vinc.append(df_tl.iloc[i][0])
              
       self.vout=[] # Vecinos incidentes
       for i in jm[df.loc[df['id']==self.sem.attrib['id']].index[0],:].nonzero()[1]:
           if i<df_tl.shape[0]:
               self.vout.append(df_tl.iloc[i][0])
       ## Obtenemos las fases de los otros semaforos         
       self.L(root)
       self.accionelegida={}
       self.ultimoestado=list(self.Listaytipo().items())[-1][0]
       self.Acciones(desfa)
       ## Aproximador
       self.fc1 = nn.Linear(in_features=len(self.fases), out_features=2*len(self.fases))  
       self.fc2 = nn.Linear(in_features=2*len(self.fases), out_features=2*len(self.accionred))
       self.out = nn.Linear(in_features=2*len(self.accionred), out_features=len(self.accionred))
       
       self.Prog()
       self.estado=self.Listaytipo()
       self.Acciones(desfa)
       self.tiempoespera=0
       self.tecoloracion={}
       self.Carreteras(dedges)
       self.memory=ReplayMemory(10000)
       self.fasesant=self.fases
       self.optimizer = optim.Adam(params=self.parameters(), lr=lr)
    
       #     self.estado.append(selfsita)
       # in_features=


  
## Extracciones de fases que cambiar, ya que hay algunas que son necesarias para mantener la seguridad  
# También se extrae la longitud del ciclo                                  
   def Listaytipo(self):
        d1={};
        i=0;
        d1.update({"offset":float(self.sem.get("offset"))})
        self.ciclo=0
        self.programa=[]
        self.programacambios=[]
        for fase in self.sem.iter('phase'):
            self.ciclo+=float(fase.get("duration"))
            if float(fase.get("duration"))>9:
                d1.update({fase.get('state'):float(fase.get("duration"))})
                self.programacambios.append(i)
                           
            i=i+1
        return d1        
   def Prog(self):
        i=0
        for fase in self.sem.iter('phase'):
            self.programa.append({fase.get('state'):float(fase.get("duration"))})
           

        

##  Extraccion de fases vecinas y propias.
   def L(self,root):
        self.fases=[]
        self.fases.append(float(self.sem.get("offset")))
        for fase in self.sem.iter('phase'):
            if float(fase.get("duration"))>9:
                self.fases.append(float(fase.get("duration")))
        for i in self.vinc:
            a='./tlLogic[@id="'+i+'"]'
            a1=root.findall(a)[0]
            self.fases.append(float(a1.get("offset")))
            for fase in a1.iter('phase'):
                if float(fase.get("duration"))>9:
                    self.fases.append(float(fase.get("duration")))
## Extraccion de acciones posibles. Estas vienen dadas en forma de lista, donde cada entrada es un diccionario, el cual contine 
# suma que se le tiene que hacer a cada fase, menos a la ultima.
   def Acciones(self,margen):
       dic1={}
       a=self.Listaytipo()
       del a[self.ultimoestado]
       for p in margen:
           l=0
           m=0
           o=l
           op=pow(5,len(a))-1
           opb3=len(np.base_repr(op,base=5))
           b1={}
           v=[]
           lacc=list(a)
           for i in range(op+1):
               k=np.base_repr(i,base=5,padding=opb3-len(np.base_repr(i,base=5))+2)[::-1]
               for j in range(opb3):
                   b1.update({lacc[j]:[-int(p),-int(p)/2,0,int(p),p][int(k[j])]  })           
               v.append(b1)
               b1={}
           dic1.update({str(int(p)):v})
           v=[]
           
       self.accionred=dic1 
       # v=list(self.accio)
       # for i in range(op+1):
           
       #     k=np.base_repr(i,base=3,padding=opb3-len(np.base_repr(i,base=3)))
       #     for j in range(opb3):
       #         b.update({v[j]:[-0.5,0,0.5][int(k[j])]  })
               
            
       #     self.acciored.update({i:b})
       #     b={}

           
## Cambiar el estado. Primero z                
   def Efectuaraccion(self,margen,rate):
       x=0
       a=[]
       for i in self.accionelegida:
           if i=="offset":
               q=self.accionelegida[i]               
           else:
               q=self.accionelegida[i]
               x=x+q 
               a.append((q+self.estado[i]))
       a.append(self.estado[self.ultimoestado]-x)

       
       if all(i >= 10 for i in a):
           x=0
           for i in self.accionelegida:
               if i=="offset":
                   q=self.accionelegida[i]
                   self.estado[i]=((q+self.estado[i]) % self.ciclo)
               else:
                   q=self.accionelegida[i]
                   x=x+q
                   self.estado[i]=((q+self.estado[i]))
           self.estado[self.ultimoestado]=self.estado[self.ultimoestado]-x
          

       else:
            self.select_action(margen,1)
            self.Efectuaraccion(margen,1)
       
            # Posible bug numeros negativos.
   def select_action(self,margen,rate):
       self.action=randrange(len(self.accionred[str(int(margen))]))
       self.accionelegida=self.accionred[str(int(margen))][self.action] # explore  
      
   # def select_action(self,margen):
   #     for i in margen:
   #         if rate > random.random():
   #             self.action=randrange(len(self.accionred[str(i)]))
   #             self.accionelegida=self.accionred[self.action]
   #         else:
   #             with torch.no_grad():
   #                 self.action=int(self.forward(torch.FloatTensor([self.fases]).to(device)).argmax())
   #                 self.accionelegida=self.accionred[self.action] # exploit
   #             #self.accionelegida=self.acciored[int(policy_net(state).argmax(dim=1).to(self.device)[0])] # exploit
   def inicio(self, strategy, num_actions, device):
       self.current_step = 0
       self.strategy = strategy
       self.num_actions = num_actions
       self.device = device
                 
   def get_options():
      optParser = optparse.OptionParser()
      optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
      options, args = optParser.parse_args()
      return options   
    
       
   def Carreteras(self,dedges):
       self.aristas=[]
       for i in self.vinc:
           self.aristas.append(dedges.at[dedges.loc[(dedges["from"]==i)&(dedges["to"]==self.nombre)].index[0],"id"])
           
   def Tiempo_espera_interseccion(self):
       for i in self.aristas:
           self.tiempoespera+=traci.edge.getWaitingTime(i)
           
           
   def Recompensa(self,Sems):
       self.recom=-self.tiempoespera
       for i in self.vinc:
           self.recom+=-Sems[i].tiempoespera/len(self.vinc)
       for i in self.vout:
           self.recom+=-Sems[i].tiempoespera/len(self.vout)
       return self.recom
# %%   
def Extraccion(nombre_mapa): 
    dom=ET.parse(nombre_mapa)
    root = dom.getroot()
    df=pd.DataFrame(columns=['id','x','y'])
    junction=pd.DataFrame(columns=['id','x','y'])
    ## Junctions
    v=root.findall('./junction[@shape]')
    for elm in v:
        junction=junction.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)
    
    ## Traffic light
    v=root.findall('./junction[@type="traffic_light"]')
    for elm in v:
        df=df.append({'id': elm.attrib['id'],'x':elm.attrib['x'],'y':elm.attrib['y']}, ignore_index=True)
    df_tl=df.copy()
    for elm in root.findall('./edge[@to]'):
        if df_tl.loc[df_tl['id'] == elm.attrib["to"]].empty  == False:
            df=df.append({'id': elm.attrib["from"]}, ignore_index=True)
    df=df.drop_duplicates(subset=['id'], keep='first')
    df=df.reset_index()
    
    
    
    m=df_tl.shape[0]
    n=df.shape[0]
    Matriz_semaforos = csr_matrix((m,m ),dtype = np.int8).toarray()
    Matriz_semaforos=coo_matrix(Matriz_semaforos)
    Matriz_semaforos=Matriz_semaforos.tocsr()
    
    for elm in root.findall('./edge[@to]'):
        if df_tl.loc[df_tl['id'] == elm.attrib["from"]].empty == False and df_tl.loc[df_tl['id'] == elm.attrib["to"]].empty==False:
    
            Matriz_semaforos[ df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0],df_tl.loc[df_tl['id']==elm.attrib["to"]].index[0]]=1
            # js[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
            # jl[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['length'])
            # jv[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['speed'])
            # jc[df_tl.loc[df_tl.loc[df_tl['id']==elm.attrib["from"]].index[0], df_tl['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
    
    ##Añadir x, y
    for i in list(df.id):
        df.loc[df['id']==i,'x':'y']=junction.loc[junction['id']==i,'x'].iloc[0],junction.loc[junction['id']==i,'y'].iloc[0]
    ## Matriz de Conexion de intersecciones
    JM = csr_matrix((n, n),dtype = np.int8).toarray()
    ## Matriz de conexion con numero de carriles
    JC = csr_matrix((n, n),dtype = np.int8).toarray()
    ## Matriz de posicion de semaforo en la intersecciones
    JS = csr_matrix((n, n),dtype = np.int8).toarray()
    ## Matriz de longitud de segmento
    JL = csr_matrix((n, n),dtype = np.float).toarray()
    ## Matriz de velocidad
    JV = csr_matrix((n, n),dtype = np.float).toarray()
    ## Matriz de velocidad
    JI = csr_matrix((n, n),dtype = np.float).toarray()
    JM=coo_matrix(JM)
    JC=coo_matrix(JC)
    JS=coo_matrix(JS)
    JL=coo_matrix(JL)
    JV=coo_matrix(JV)
    JI=coo_matrix(JI)
    jm=JM.tocsr()
    js=JS.tocsr()
    jl=JL.tocsr()
    jv=JV.tocsr()
    ji=JI.tocsr()
    jc=JC.tocsr()
    dedges=pd.DataFrame(columns=['id','from','to','index'])
    for elm in root.findall('./edge[@from]'):
        dedges=dedges.append({'id': elm.attrib["id"],'from':elm.attrib["from"],'to':elm.attrib["to"],'index':[]}, ignore_index=True)
    for elm in root.findall('./edge[@to]'):
        if df.loc[df['id'] == elm.attrib["from"]].empty == False and df.loc[df['id'] == elm.attrib["to"]].empty==False:
    
            jm[ df.loc[df['id']==elm.attrib["from"]].index[0],df.loc[df['id']==elm.attrib["to"]].index[0]]=1
            # js[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
            # jl[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['length'])
            # jv[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=float(elm.getchildren()[0].attrib['speed'])
            # jc[df.loc[df.loc[df['id']==elm.attrib["from"]].index[0], df['id']==elm.attrib["to"]].index[0]]=len(elm.getchildren())
    
    for elm in root.findall('./connection[@tl]'):
        dedges.iloc[dedges.iloc[(dedges['id'] == elm.attrib["from"]).values,[3]].index[0]][3].append(elm.attrib["linkIndex"])

    dedges.iloc[1][3].append(1)
    def inverse_mapping(f):
        return f.__class__(map(reversed, f.items()))
    
    G=nx.from_scipy_sparse_matrix(Matriz_semaforos, parallel_edges=False, create_using=nx.DiGraph)
    Colors={}
    listcoloring=nx.coloring.greedy_color(G)
    b=nx.coloring.greedy_color(G)
    for k in b:
        listcoloring[df_tl.iloc[k][0]]=listcoloring.pop(k)
    
    coloringlist = {}
    for k, v in listcoloring.items():
        coloringlist[v] = coloringlist.get(v, []) + [k]
    return jm,df,df_tl,root,dedges,coloringlist

class EpsilonGreedyStrategy():
    def __init__(self, start, end, decay):
        self.start = start
        self.end = end
        self.decay = decay
        
    def get_exploration_rate(self, current_step):
        return self.end + (self.start - self.end) * \
        math.exp(-1. * current_step * self.decay)
    

def Accionrama(rama,alpha,hoja,episode,timestep,Sems,Paccion,dom):
    rate=1
    if hoja==0 and episode==0 and timestep==0:
        shutil.copy2("/home/jj/Documents/tfm/mapasyrutas/Mapa_volumen"+str(alpha)+"rama"+str(rama)+".net.xml", "/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+".net.xml")
    elif hoja==0:
        shutil.copy2("/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+".net.xml", "/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+".net.xml")
    else:
        
        for i in Sems:
            Sems[i].select_action(Paccion[0],rate)
            Sems[i].Efectuaraccion(Paccion[0],rate)
            Sems[i].sem.set("offset", str(Sems[i].estado["offset"]))
            for j in Sems[i].sem.iter("phase"):
                if j.attrib["state"] in Sems[i].estado:
                    j.set("duration", str(Sems[i].estado[j.attrib["state"]]))
            dom.write("/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+".net.xml")

def sumo(conection,alpha,hoja,rama,df_tl, Sems):
    # print("Hello")
    # time.sleep(1)
    a={}
    traci.start(["sumo", "-c", '/home/jj/Documents/tfm/mapasyrutas/sim_volumen'+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+'.sumocfg','--no-warnings',   "--no-step-log",
                                  "--tripinfo-output", "tripinfo.xml"],label=conection)
    con=traci.getConnection(conection);
    step = 0
    a={}
    while 700> step:
        con.simulationStep();
        step += 1
        for i in df_tl["id"]:
            Sems[i].Tiempo_espera_interseccion();
    a=0     
    for i in Sems:
        a+=Sems[i].tiempoespera;

    traci.switch(conection)
    traci.close()
    return a;
        
# def sumo():
#       if __name__ == "__main__":
#           options = get_options()
     
#           # this script has been called from the command line. It will start sumo as a
#           # server, then connect and run
#           if options.nogui:
#               print("nogui")
#               sumoBinary = checkBinary('sumo')
#           else:
#               print("gui")
#               sumoBinary = checkBinary('sumo-gui')
     
#           # first, generate the route file for this simulation
     
#           # this is the normal way of using traci. sumo is started as a
#           # subprocess and then the python script connects and runs
#           traci.start(["sumo", "-c", "/home/juan/Documentos/Tfm/sim.sumocfg",
#                                   "--tripinfo-output", "tripinfo.xml"])
#           step = 0
#           while 700> step:
#               traci.simulationStep()
#               for i in df_tl["id"]:
#                   Sems[i].Tiempo_espera_interseccion()
                 
     
#               step += 1
#       traci.close()

class QValues():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    @staticmethod
    def get_current(policy_net, states, actions):
        return policy_net(states).gather(dim=1, index=actions.unsqueeze(-1))
    @staticmethod        
    def get_next(target_net, next_states):                
        final_state_locations = False
        non_final_state_locations = (final_state_locations == False)
        non_final_states = next_states[non_final_state_locations]
        batch_size = next_states.shape[0]
        values = torch.zeros(batch_size).to("cuda")
        values[non_final_state_locations] = target_net(non_final_states).max(dim=1)[0].detach()
        return values
    
 
def argmax(lst):
  return lst.index(max(lst))

# %%
def Buckle_aprendizaje(ramas,alpha,Paccion,df_tl,Sems,num_episodes,num_timestep,nombre_config,jm,df,dedges):
    st = time.time()
    len_rama=len(ramas[1])
    values=[]
    Paccion_or=Paccion
    for rama in ramas:
        domcfg=ET.parse(nombre_config)
        rootcfg = domcfg.getroot()
        for hoja in ramas[rama]:
            for i in rootcfg.iter("net-file"):
                i.set("value", "/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+".net.xml")
            # for i in rootcfg.iter("additional-files"):
                # i.set("value", "/home/jj/Documents/tfm/mapasyrutas/valencia"+".rou.valenciaATA.xml , /home/jj/Documents/tfm/mapasyrutas/valencia"+".add.valenciaATA.xml")
            domcfg.write('/home/jj/Documents/tfm/mapasyrutas/sim_volumen'+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+'.sumocfg')
    #     shutil.copy2("/home/jj/Documents/tfm/mapasyrutas/valencia1.net.xml","/home/jj/Documents/tfm/mapasyrutas/Mapa_volumen"+str(alpha)+"rama"+str(rama)+".net.xml")
    ayuda={}
    ayuda_ant={}
    for episode in range(num_episodes):
        for timestep in range(num_timestep):
            if (timestep + 200) % 199 == 0:
                time.sleep(20)
            Paccion=[(1 +  x*math.exp(-1. * (episode * num_timestep+timestep)*0.085)) for x in Paccion_or]
            resultados=[[] for _ in range(len(ramas))]
            for rama in ramas:
                res=[]
                if episode==0 and timestep==0:
                    file_name="/home/jj/Documents/tfm/mapasyrutas/valencia1.net.xml"
                    dom=ET.parse(file_name)
                    root = dom.getroot()
                    for i in df_tl["id"]:
                        a='./tlLogic[@id="'+i+'"]'
                        Sems.update({i:Extr(root.findall(a)[0],Paccion,jm,df,df_tl,root,dedges).to(device)})
                else:
                    file_name="/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+".net.xml"
                    dom=ET.parse(file_name)
                    root = dom.getroot()
                    for i in df_tl["id"]:
                        a='./tlLogic[@id="'+i+'"]'
                        Sems.update({i:Extr(root.findall(a)[0],Paccion,jm,df,df_tl,root,dedges).to(device)})

                for hoja in ramas[rama]:
                    Accionrama(rama,alpha,hoja,episode,timestep,Sems,Paccion,dom)
                    file_name="/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+".net.xml"
                    dom=ET.parse(file_name)
                    root = dom.getroot()
        
                    for i in df_tl["id"]:
                        a='./tlLogic[@id="'+i+'"]'
                        Sems[i].sem=root.findall(a)[0]
                    res.append( sumo("conection"+str(hoja),alpha,hoja,rama,df_tl, Sems) )

                hoja=argmax(res)
                shutil.copy2("/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+"hoja"+str(hoja)+".net.xml","/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+".net.xml")
       
    for rama in ramas:
        shutil.copy2("/dev/shm/Mapa_volumen"+str(alpha)+"rama"+str(rama)+".net.xml"".net.xml","/home/jj/Documents/tfm/mapasyrutas/Mapa_volumen"+str(alpha)+"rama"+str(rama)+".net.xml")
       
    
    
    
    
    # label=[]
    # for k in coloringlist:
    #     label.append(("a"+str(k),"b"+str(k)))
                
    # def helper(numbers):
    #     return sumo(numbers[0], numbers[1])
    # values=[]
    # label=(("sim1",0),("sim2",1),("sim3",2))
    # if __name__ == '__main__':
    #     with concurrent.futures.ProcessPoolExecutor() as executor:
    #                 for result in executor.map(helper, label):
    #                     values.append(result)
    fn= time.time()

    print(fn-st)
    

# %%

def main(alpha):
    ramas={}
    for i in range(4):
        ramas.update({i:range(7)})
    nombre_mapa='/home/jj/Documents/tfm/mapasyrutas/valencia1.net.xml'
    nombre_ruta='/home/jj/Documents/tfm/mapasyrutas/valencia.rou.valenciaATA.xml'
    nombre_add="/home/jj/Documents/tfm/mapasyrutas/valencia.add.valenciaATA.xml"
    nombre_config="/home/jj/Documents/tfm/mapasyrutas/sumo.sumocfg"
    num_timestep=2
    num_episodes=20
    
    jm,df,df_tl,root,dedges,coloringlist=Extraccion(nombre_mapa)
    Sems={}
    Paccion=[10]
    for i in df_tl["id"]:
        a='./tlLogic[@id="'+i+'"]'
        Sems.update({i:Extr(root.findall(a)[0],Paccion,jm,df,df_tl,root,dedges).to(device)})


    print(len(coloringlist))
    Buckle_aprendizaje(ramas,alpha,Paccion,df_tl,Sems,num_episodes,num_timestep,nombre_config,jm,df,dedges)
           
if __name__=="__main__":
    main(1)